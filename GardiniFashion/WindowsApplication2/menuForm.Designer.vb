﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class menuForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(menuForm))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.inventoryBtn = New System.Windows.Forms.Button()
        Me.oldTrans = New System.Windows.Forms.Button()
        Me.newTrans = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(135, 41)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(326, 102)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(140, 146)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(321, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "1229 Espana cor. P. Noval St., Sampaloc, Manila"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(231, 164)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(122, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tel No : 263-3141"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(172, 182)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(258, 15)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cell Nos. 09196673397 - 09184086052"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.inventoryBtn)
        Me.GroupBox1.Controls.Add(Me.oldTrans)
        Me.GroupBox1.Controls.Add(Me.newTrans)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.GroupBox1.Location = New System.Drawing.Point(128, 253)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(326, 124)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Menu"
        '
        'inventoryBtn
        '
        Me.inventoryBtn.BackColor = System.Drawing.Color.Gray
        Me.inventoryBtn.ForeColor = System.Drawing.Color.White
        Me.inventoryBtn.Location = New System.Drawing.Point(119, 75)
        Me.inventoryBtn.Name = "inventoryBtn"
        Me.inventoryBtn.Size = New System.Drawing.Size(99, 43)
        Me.inventoryBtn.TabIndex = 2
        Me.inventoryBtn.Text = "Inventory"
        Me.inventoryBtn.UseVisualStyleBackColor = False
        '
        'oldTrans
        '
        Me.oldTrans.BackColor = System.Drawing.Color.Gray
        Me.oldTrans.ForeColor = System.Drawing.Color.White
        Me.oldTrans.Location = New System.Drawing.Point(196, 21)
        Me.oldTrans.Name = "oldTrans"
        Me.oldTrans.Size = New System.Drawing.Size(99, 43)
        Me.oldTrans.TabIndex = 1
        Me.oldTrans.Text = "Old Transaction"
        Me.oldTrans.UseVisualStyleBackColor = False
        '
        'newTrans
        '
        Me.newTrans.BackColor = System.Drawing.Color.Gray
        Me.newTrans.ForeColor = System.Drawing.Color.White
        Me.newTrans.Location = New System.Drawing.Point(31, 21)
        Me.newTrans.Name = "newTrans"
        Me.newTrans.Size = New System.Drawing.Size(99, 43)
        Me.newTrans.TabIndex = 0
        Me.newTrans.Text = "New Transaction"
        Me.newTrans.UseVisualStyleBackColor = False
        '
        'menuForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(584, 461)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "menuForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gardini Fashion Rental"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents oldTrans As System.Windows.Forms.Button
    Friend WithEvents newTrans As System.Windows.Forms.Button
    Friend WithEvents inventoryBtn As System.Windows.Forms.Button

End Class
